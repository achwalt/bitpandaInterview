import requests
from datetime import datetime
import time
from tqdm import tqdm

def to_datetime(string):
    return datetime.strptime(string, "%Y-%m-%dT%H:%M:%SZ")

def safe(dict_, keys:list):
    extract = dict_
    for key in keys:
        try: extract = extract[key]
        except TypeError: extract = 0
        except KeyError: extract = 0
    return extract

def tweets_for_ticker(ticker = "MVST"):
    "Scrapes 30 tweets for each ticker"
    # API Docu is currently under review: https://api.stocktwits.com/developers
    headers = {"user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0"}
    url = f"https://api.stocktwits.com/api/2/streams/symbol/{ticker}.json?filter=top&limit=30"
    json = requests.get(url, headers = headers).json()
    dataset = []
    if json["response"]["status"] == 200:
        for r in json["messages"]:
            if r:
                row = {}
                row["ticker"] = ticker
                row["date"] = to_datetime(safe(r, ["created_at"]))
                row["id"] = safe(r, ["id"])
                row["user"] = safe(r, ["user", "username"])
                row["followers"] = safe(r, ["user", "followers"])
                row["content"] = safe(r, ["body"])
                row["sentiment"] = safe(r, ["entities", "sentiment", "basic"])
                row["likeCount"] = safe(r, ["likes", "total"])
                dataset.append(row)
    elif json["response"]["status"] == 429:
        dataset = "WAIT" # API rate limit applied
    else:
        dataset = "ERROR"
    return dataset

def get_sentiment_dataset(list_of_tickers):
    dataset = []
    start = datetime.now().timestamp()
    for ticker in tqdm(list_of_tickers, desc = "Scraping StockTwits"):
        result = tweets_for_ticker(ticker)
        if type(result) == list:
            dataset.extend(result)
        elif result == "WAIT":
            now = datetime.now().timestamp()
            time.sleep(max(5*60, 60 * 60 - (now-start))) # Wait for rate limit to disappear
            start = datetime.now().timestamp()
        else: pass # Probably ticker wasn't found
    return dataset

if __name__ == "__main__":
    print("Don't run this file")
    
    """
    import os
    from stocks import Stocks
    st = Stocks()
    tickers = st.load_data().columns
    tickers = [i.replace(" Adj Close", "") for i in tickers if "Adj Close" in i]
    dataset = get_sentiment_dataset(tickers)
    dataset_path = os.path.join(".sentiments", "sentiment_dataset.csv")
    try:
        df = pd.DataFrame(dataset)
        df.to_csv(dataset_path, encoding = "utf-8-sig")
    except:
        import yaml
        with open(os.path.splitext(dataset_path)[0] + ".yml", "w", encoding = "utf-8-sig") as f:
            yaml.dump(dataset, f)
    """
    
    # -------------------------------------------------------------------------
    
    """
    from sentiment_transformers import SentimentTransformers
    from data import Data
    sent = SentimentTransformers()
    src_data = Data().load()
    sentiments = []
    for content in tqdm(src_data.content.values, desc = "Extract Sentiments"):
        sentiments.append(sent.predict(content))
    incl_sent = src_data
    incl_sent["sentiment"] = sentiments
    incl_sent.to_csv(".sentiments/src_data_incl_sentiments.csv", encoding = "utf-8-sig")
    """        