import multiprocessing as mp
import time
from tqdm import tqdm
import yaml

def run_procs(list_of_procs, desc = "Running Procs", limit = None, daemon = True, wait = False):
    started_procs = []
    limit = mp.cpu_count() if not limit else limit
    for p in tqdm(list_of_procs, desc = desc):
        p.daemon = daemon
        p.start()
        started_procs.append(p)
        while len(started_procs) >= limit:
            for p in started_procs:
                p.join(timeout = 0.01)
                if p.is_alive():
                    try: p.close()
                    except: pass # Python < 3.7
                    started_procs.remove(p)
        if wait: time.sleep(wait)
    for p in started_procs:
        p.join()
    for p in started_procs:
        try: p.close()
        except: pass
        started_procs.remove(p)
        
def load_mapping(fpath = ".mapping"):
    with open(fpath, "r", encoding = "utf-8-sig") as f:
        mapping = yaml.safe_load(f)
    return mapping