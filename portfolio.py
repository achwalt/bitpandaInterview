import pandas as pd
import numpy as np
from tqdm import tqdm
from matplotlib import pyplot
import os

from data import Data
from stocks import Stocks

class Portfolio:

    def __init__(self):
        self.stocks = Stocks()
        self.data_folder = ".data"
        self.benchmark_data_fname = "spx.csv"
    
    @staticmethod
    def tf_idf(word, list_of_mentions):
        "term frequency by inverse document frequency"
        # We dont want to use this in this context
        term_freq = 0
        doc_freq = 0
        word = word.strip().lower()
        for item in list_of_mentions:
            doc_ = False
            for w in item:
                if w.strip().lower() == word:
                    term_freq += 1
                    if not doc_:
                        doc_freq += 1
                        doc_ = True
        return term_freq * np.log(len(list_of_mentions)/doc_freq)
    
    @staticmethod
    def tf_df(word, list_of_mentions):
        "term frequency by document frequency"
        # IDEA: Oftenly mentioned stocks might be favorable
        # Computationally expensive
        term_freq = 0
        doc_freq = 0
        word = word.strip().lower()
        for item in list_of_mentions:
            doc_ = False
            for w in item:
                if w.strip().lower() == word:
                    term_freq += 1
                    if not doc_:
                        doc_freq += 1
                        doc_ = True
        return term_freq * np.log(doc_freq)
    
    @staticmethod
    def get_trading_times(datetime_date, starting_hour, closing_hour):
        start = datetime_date.replace(hour = starting_hour, minute = 0, second = 0)
        stop = datetime_date.replace(hour = closing_hour, minute = 0, second = 0)
        return start, stop
    
    @staticmethod
    def apply_cutoffs(dataset):
        df = dataset[dataset.retweetCount > 0]
        df = df[df.likeCount > df.likeCount.quantile(0.5)]
        return df
    
    def count_ticker_appearance(self, dataset, list_of_tickers, found_tickers = None, return_found_tickers = False):
        if not found_tickers: found_tickers = self.stocks.extract_tickers_from_list(dataset.content)
        appearances = {}
        for ticker in list_of_tickers:
            appearances[ticker] = 0
        for row in found_tickers:
            for ticker in row:
                try: appearances[ticker] += 1
                except: pass
        if return_found_tickers: return pd.Series(appearances, name = "appearances"), found_tickers
        else: return pd.Series(appearances, name = "appearances")
    
    def ticker_appears_in_contents(self, dataset, ticker, found_tickers = None):
        # TODO: Use Sparse Matrix instead
        if not found_tickers: found_tickers = self.stocks.extract_tickers_from_list(dataset.content)
        result = np.zeros(len(dataset.index), dtype = bool)
        for i, row in enumerate(found_tickers):
            if ticker in row:
                result[i] = True
        return result
    
    def calculate_ranking(self, dataset, list_of_tickers, verbose = False):
        # First, only use tweets with positive retweetCount
        # From these, obtain a median cutoff for the likeCount
        # Only consider the upper halve
        log_likes = np.log(1 + dataset.likeCount) # Likes are positive
        log_comments = np.log(1 + dataset.replyCount) # Comments are probably more negative
        dataset["likeCommentRatio"] = log_likes/log_likes.max() - log_comments/log_comments.max()
        log_retweets = np.log(1 + dataset.retweetCount)
        dataset["retweetWeight"] = log_retweets/log_retweets.max()
        score = pd.Series(np.zeros(len(list_of_tickers)), name = "score", index = list_of_tickers)
        appearances, found_tickers = self.count_ticker_appearance(dataset, list_of_tickers, None, True)
        iterator = tqdm(list_of_tickers) if verbose else list_of_tickers
        for ticker in iterator:
            ticker_in_content = self.ticker_appears_in_contents(dataset, ticker, found_tickers)
            score[ticker] = appearances[ticker] * dataset.likeCommentRatio[ticker_in_content].mean() * dataset.retweetWeight[ticker_in_content].mean()
        return score.sort_values(ascending = False)
    
    @staticmethod
    def check_matching_day(datetime_date, datetime_index):
        for datetime2 in datetime_index:
            if datetime_date.day == datetime2.day:
                if datetime_date.month == datetime2.month:
                    if datetime_date.year == datetime2.year:
                        return datetime2
        else: return None
    
    def compare_list_elements(self, list_a, list_b, invert = False):
        ret = np.zeros(len(list_a), dtype = np.bool) if not invert else np.ones(len(list_a), dtype = np.bool)
        for i, item in enumerate(list_a):
            if item in list_b:
                ret[i] = True if not invert else False
        return np.array([i for i in ret], dtype = np.bool)
    
    def calc_required_trades(self, this_periods_assets, last_periods_assets, last_periods_returns):
        portfolio_values = 1 + last_periods_returns
        target_value = 1 + last_periods_returns.mean()
        required_trades = portfolio_values/target_value - 1
        full_liquidations = self.compare_list_elements(last_periods_assets, this_periods_assets, True)
        execute_trades = []
        for a, asset in enumerate(full_liquidations):
            if asset:
                execute_trades.append(1)
            else:
                execute_trades.append(required_trades[a])
        return np.array(execute_trades)
    
    def ensure_list_len(self, list_a, list_b):
        if len(list_a) == len(list_b):
            return list_a
        else:
            list_clone = list_a
            diff = len(list_b) - len(list_a)
            for i in range(diff):
                try: list_clone.append(list_a[i])
                except IndexError:
                    j = abs(i - len(list_a))
                    list_clone.append(list_a[j])
        return list_clone
    
    @staticmethod
    def portfolio_characteristics(series_of_total_returns, annual_correction = 252):
        returns = series_of_total_returns/series_of_total_returns.shift().values
        ret = {}
        ret["GeoMean"] = ((1 * returns).prod() ** (1/len(returns)) - 1) * annual_correction 
        ret["Vola"] = returns.std() * (annual_correction ** (0.5))
        ret["Sharpe"] = ret["GeoMean"]/ret["Vola"] # No risk-free-rate
        return ret
    
    def total_return_20_stocks(self, dataset, stock_prices, list_of_tickers = None, require_sentiment = None, starting_hour = 8, closing_hour = 22, trading_delay_hours = 1, correct_timezone = 0, verbose = True): # Trading times from gettex
        """
        ASSUMPTIONS:
        - Trading times are not derived from yahoo since there is no clarity about when they are taken (Open: 02:00, Close: 00:00): 
          --> https://stackoverflow.com/questions/65387258/yahoo-finance-filling-up-the-two-hour-delay
        - The longest open exchange known to me is gettex from 8:00 to 22:00
        - Therefore I will pretend as if trading would be possible between these times
        - Timezones: Yahoo is daily data w/o a timezone attached. Since prices are sampled in UTC, CET would need a correction -2 hours
        - Trades are done at the end and the beginning of each day.
        - There is no short trading
        - The portfolio is 1/N of the 20 most mentioned assets
        - We have Adj Closed prices => Allow for 1h delay => Tweets must arrive before closing_hour - 1
        - Adjusted Open prices are open prices plus times 1 + arithmetic outperformance of adj close over close of previous day
        """
        open_price, close_price = self.stocks.adjust_open(stock_prices.dropna(how = "all"))
        open_return, close_return = self.stocks.get_returns(open_price), self.stocks.get_returns(close_price)
        
        # Buy at market closing after observing tweets
        # If you assume selling at the close and buying next morning
        # cost_per_trade = (open_price.shift(-1)/close_price.values - 1).fillna(0) * (-1) # TODO: Uncomment this line if you wish
        # cost_per_trade.columns = close_price.columns
        # Or receive a simulated flash loan which is almost free to do simultaneous trading
        np.random.seed(42)
        cost_per_trade = pd.DataFrame(np.random.uniform(-0.001, -0.003, size = open_price.shape),
                                           index = close_price.index,
                                           columns = close_price.columns) # Efffectively 10 bps
        last_stocks_20 = None
        
        if not list_of_tickers: list_of_tickers = set(["$" + i.split()[0] for i in close_price.columns])
        dataset = self.apply_cutoffs(dataset) # We can save on computing by applying cutoffs ex ante
        dataset.index = pd.to_datetime(dataset.date, utc = 1).dt.tz_convert("Europe/Vienna") # TZ-Handling
        dataset = dataset.sort_index()
        
        start_day, _ = self.get_trading_times(dataset.index.min(), starting_hour, closing_hour)
        _, stop_day = self.get_trading_times(dataset.index.max(), starting_hour, closing_hour)
        
        total_days = (stop_day - start_day).days
        
        total_gross_return_index = []
        total_net_return_index = []
        iterator = tqdm(range(total_days + 1), desc = "Gen TotalReturn-Index") if verbose else range(total_days + 1)
        for days_passed in iterator:
            from_date = start_day + pd.Timedelta(days = days_passed)
            to_date = from_date + pd.Timedelta(hours = closing_hour - starting_hour - trading_delay_hours)
            today = self.check_matching_day(from_date, open_return.index)
            if today: # if price data for today exists
                subdata = dataset[dataset.index >= from_date]
                subdata = subdata[subdata.index <= to_date]
                if require_sentiment is not None: 
                    subdata = subdata[subdata.sentiment == require_sentiment]
                stocks_20 = [f"{i[1:]} Adj Close" for i in self.calculate_ranking(subdata, list_of_tickers, verbose = False).dropna().index[:20]]
                if len(stocks_20) < 20: stocks_20 = self.ensure_list_len(stocks_20, last_stocks_20)
                todays_return = close_return.loc(0)[today].loc(0)[stocks_20].mean() # Arithmetic mean for 1/N weighting operations
                if last_stocks_20 is not None:
                    required_trades = self.calc_required_trades(stocks_20, last_stocks_20, close_return.shift().loc(0)[today].loc(0)[last_stocks_20])
                else:
                    required_trades = np.ones(20)
                trade_cost = (cost_per_trade.loc(0)[today].loc(0)[stocks_20] * np.abs(required_trades)).mean()
                total_gross_return_index.append(todays_return)
                total_net_return_index.append(todays_return + trade_cost)
                last_stocks_20 = stocks_20
        total_gross_return_index = (1 + pd.Series(total_gross_return_index, index = close_price.index, name = "grossReturn")).cumprod()
        total_net_return_index = (1 + pd.Series(total_net_return_index, index = close_price.index, name = "netReturn")).cumprod()
        return total_gross_return_index, total_net_return_index

    def plot_results(self, total_gross_return_index, total_net_return_index, title = "Backtest Results"):
        spx = (self.stocks.get_returns(self.stocks.load_data(os.path.join(self.data_folder, self.benchmark_data_fname))["^GSPC Adj Close"], False).dropna() + 1).cumprod()
        # spx.index = total_gross_return_index.index
        pyplot.figure()
        pyplot.title(title)
        pyplot.plot(total_gross_return_index)
        pyplot.plot(total_net_return_index)
        pyplot.plot(spx)
        pyplot.grid()
        pyplot.xticks(rotation = 30)
        pyplot.legend(["Before TC & Delay", "After TC & Delay", "SPX"])
        return total_gross_return_index, total_net_return_index, spx
    
if __name__ == "__main__":
    data = Data()
    df = data.load()
    st = Stocks()
    contents = df["content"]
    tickers = st.extract_tickers_from_list(contents)
    unique_tickers = st.extract_tickers_from_list(contents, unique = True)
    # mentions = [tf_df(t, tickers) for t in unique_tickers]
    stock_prices = st.load_data()
    pf = Portfolio()
    gross_totret, net_totret = pf.total_return_20_stocks(df, stock_prices)
    _, _, spx = pf.plot_results(gross_totret, net_totret, "Backtest w/o Sentiment")
    # Positive Sentiment
    df_sent = pd.read_csv(os.path.join(".sentiments", "src_data_incl_sentiments.csv"))
    gross_totret_pos, net_totret_pos = pf.total_return_20_stocks(df_sent, stock_prices, require_sentiment = "Bullish")
    _, _, spx = pf.plot_results(gross_totret, net_totret, "Backtest on positive Sentiment")
    gross_totret_neg, net_totret_neg = pf.total_return_20_stocks(df_sent, stock_prices, require_sentiment = "Bearish")
    _, _, spx = pf.plot_results(gross_totret, net_totret, "Backtest on negative Sentiment")
    
    print("Default Portfolio", pf.portfolio_characteristics(gross_totret, len(gross_totret)))
    print("Positive Sentiment Portfolio", pf.portfolio_characteristics(gross_totret_pos, len(gross_totret_pos)))
    print("Negative Sentiment Portfolio", pf.portfolio_characteristics(gross_totret_neg, len(gross_totret_neg)))
    print("S&P 500:", pf.portfolio_characteristics(spx, len(spx)))