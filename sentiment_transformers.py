#IMPORT Section----------------------------------------------------------------

import random
import numpy as np

import torch
from transformers.file_utils import is_tf_available, is_torch_available

from transformers import BertTokenizerFast, BertForSequenceClassification
from transformers import Trainer, TrainingArguments


from sklearn.metrics import accuracy_score, f1_score

import pandas as pd

import re

import os

import yaml


#MAIN--------------------------------------------------------------------------

class ToTorchDataset(torch.utils.data.Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        item = {k: torch.tensor(v[idx]) for k, v in self.encodings.items()}
        item["labels"] = torch.tensor([self.labels[idx]])
        return item

    def __len__(self):
        return len(self.labels)

class SentimentTransformers:
    
    def __init__(self):
        #CONFIG
        self.device = "cuda:0"
        self.set_seed(1)
        self.model_name = "prajjwal1/bert-small-mnli"
        self.max_length = 512
        self.path_to_sentiment_data = os.path.join(".sentiments","sentiment_dataset.csv")
        self.train_test_split = 0.2
        self.save_new_sentiments_mapping = False
        self.target_model_path = os.path.join(".sentiments", "sentiments_model")
        self.mapping_path = os.path.join(".sentiments","sentiments_mapping.yml")

        #INIT
        self.train_text = None
        self.train_labels = None
        self.valid_text = None
        self.valid_labels = None
        self.mapping = None
        self.train_dataset = None
        self.valid_dataset = None
        self.tokenizer = None
        self.model = None

        #TRAINING ARGS
        self.training_args = TrainingArguments(
            output_dir = os.path.join(".sentiments", "results"),
            num_train_epochs = 8,
            per_device_train_batch_size = 16,
            per_device_eval_batch_size = 20,
            learning_rate = 3e-05,
            warmup_steps = 500, # number of warmup steps for learning rate scheduler
            weight_decay = 0.01,
            logging_dir = './logs',
            load_best_model_at_end = True,
            logging_steps = 1000,
            save_steps = 1000,
            evaluation_strategy = "steps",
        )
    
    def load_sentiments_mapping(self):
        with open(self.mapping_path, "r", encoding = "utf-8-sig") as f:
            return yaml.safe_load(f.read())

    def save_sentiments_mapping(self, list_of_sentiments):
        sentiments = {"string": {}, "integer": {}}
        for i, sent in enumerate(set(list_of_sentiments)):
            if sent == "Bearish":
                index = 0
            elif sent == "Neutral":
                index = 1
            elif sent == "Bullish":
                index = 2
            sentiments["string"][sent] = index
            sentiments["integer"][index] = sent
        with open(self.mapping_path, "w", encoding = "utf-8-sig") as f:
            yaml.dump(sentiments, f)
        return sentiments
    
    def set_seed(self, seed: int):
        random.seed(seed)
        np.random.seed(seed)
        if is_torch_available():
            torch.manual_seed(seed)
            torch.cuda.manual_seed_all(seed) # Safe to call w/o CUDA
        if is_tf_available():
            import tensorflow as tf
            tf.random.set_seed(seed)
    
    @staticmethod
    def clean_text(string):
        string = re.sub("""((http|https)\:\/\/)?[a-zA-Z0-9\.\/\?\:@\-_=#]+\.([a-zA-Z]){2,6}([a-zA-Z0-9\.\&\/\?\:@\-_=#])*""", "", string)
        string = re.sub("@.+:", "", string)
        string = re.sub("\(", "", string)
        string = re.sub("\)", "", string)
        string = re.sub("\$[A-Z]+", "", string)
        string = " ".join(re.findall("[a-zA-Z]+ *", string)).lower()
        return string
    
    def clean_data(self, sentiments):
        sentiments.loc(1)["content"] = [self.clean_text(i) for i in sentiments.loc(1)["content"]]
        return sentiments
    
    def load_sentiments(self, path_to_sentiment_data = None, train_test_split = 0.2, save_new_sentiments_mapping = True):
        path_to_sentiment_data = path_to_sentiment_data if path_to_sentiment_data else self.path_to_sentiment_data
        sentiments = pd.read_csv(path_to_sentiment_data, encoding = "utf-8-sig").loc(1)[("content", "sentiment")]
        sentiments.loc(1)["sentiment"] = np.where(sentiments.loc(1)["sentiment"].values == "0", "Neutral", sentiments.loc(1)["sentiment"].values)
        sentiments = self.clean_data(sentiments)
        sentiments.dropna(inplace = True)
        sentiments.reset_index(drop = True, inplace = True)
        new_index = list(sentiments.index)
        
        random.shuffle(new_index)
        train_text = sentiments.loc(1)["content"].loc(0)[new_index[:int(len(new_index) * (1 - 0.2))]].to_list()
        valid_text = sentiments.loc(1)["content"].loc(0)[new_index[int(len(new_index) * (1 - 0.2)):]].to_list()
        
        mapping = self.save_sentiments_mapping(sentiments["sentiment"].to_list()) if save_new_sentiments_mapping else self.load_sentiments_mapping()
        
        train_labels = [mapping["string"][i] for i in sentiments.loc(1)["sentiment"].loc(0)[new_index[:int(len(new_index) * (1 - 0.2))]]]
        valid_labels = [mapping["string"][i] for i in sentiments.loc(1)["sentiment"].loc(0)[new_index[int(len(new_index) * (1 - 0.2)):]]]
        
        return train_text, train_labels, valid_text, valid_labels, mapping
    
    def encode(self):
        train_encodings = self.tokenizer(self.train_text, truncation=True, padding=True, max_length = self.max_length)
        valid_encodings = self.tokenizer(self.valid_text, truncation=True, padding=True, max_length = self.max_length)
        train_dataset = ToTorchDataset(train_encodings, self.train_labels)
        valid_dataset = ToTorchDataset(valid_encodings, self.valid_labels)
        return train_dataset, valid_dataset
    
    def compute_metrics(self, pred):
        labels = pred.label_ids
        preds = pred.predictions.argmax(-1)
        # calculate accuracy using sklearn's function
        acc = accuracy_score(labels, preds)
        f_macro = f1_score(labels, preds, average="macro")
        f_micro = f1_score(labels, preds, average="micro")
        return {
            'accuracy': acc,
            'f_macro': f_macro,
            'f_micro': f_micro,
        }

    def setup_train(self):
        self.save_new_sentiments_mapping = True
        self.tokenizer = BertTokenizerFast.from_pretrained(self.model_name, do_lower_case = False)
        self.train_text, self.train_labels, self.valid_text, self.valid_labels, self.mapping = self.load_sentiments(self.path_to_sentiment_data, self.train_test_split, self.save_new_sentiments_mapping)
        self.train_dataset, self.valid_dataset = self.encode()
        self.model = BertForSequenceClassification.from_pretrained(self.model_name, num_labels = len(self.mapping["string"].keys())).to(self.device)
        return self.train_dataset, self.valid_dataset

    def model_init(self):
        print("Please re-define self.model_init outside this class to use auto hyperparams tuning")
        return BertForSequenceClassification.from_pretrained(self.model_name, num_labels = len(self.mapping["string"].keys()), return_dict = True)

    def hyperparams_space(self, trial):
        from ray import tune

        return {
            "learning_rate": tune.loguniform(1e-4, 1e-1),
            "num_train_epochs": tune.choice(range(1,6)),
            "seed": tune.choice(range(1,50)),
            "per_device_train_batch_size": tune.choice([4,8,16,32,64])
        }

    def train(self, autotune = False, model_init = None):
        # load the model and pass to CUDA
        if not model_init: model_init = self.model_init
        self.setup_train()
        if autotune:
            print("Auto-tuning cannot be done inside a class - define model_init = self.model_init outside!")
            trainer = Trainer(
                args = self.training_args,                  # training arguments, defined above
                train_dataset = self.train_dataset,         # training dataset
                eval_dataset = self.valid_dataset,          # evaluation dataset
                compute_metrics = self.compute_metrics,     # the callback that computes metrics of interest
                model_init = model_init,                    # the instantiated Transformers model to be trained

            )
            trainer.hyperparameter_search(direction = "maximize", compute_objective = self.compute_metrics, hp_space = self.hyperparams_space)
        else:
            trainer = Trainer(
                model = self.model,                         # the instantiated Transformers model to be trained
                args = self.training_args,                  # training arguments, defined above
                train_dataset = self.train_dataset,         # training dataset
                eval_dataset = self.valid_dataset,          # evaluation dataset
                compute_metrics = self.compute_metrics,     # the callback that computes metrics of interest
            )
            trainer.train()
            trainer.evaluate()
        # saving the fine tuned model & tokenizer
        self.model.save_pretrained(self.target_model_path)
        self.tokenizer.save_pretrained(self.target_model_path)
       
    def setup_predict(self):
        self.save_new_sentiments_mapping = False
        if not self.tokenizer: self.tokenizer = BertTokenizerFast.from_pretrained(self.target_model_path, do_lower_case = False)
        if not self.model: self.model = BertForSequenceClassification.from_pretrained(self.target_model_path).to(self.device)
        if not self.mapping: self.mapping = self.load_sentiments_mapping()
    
    def predict(self, string):
        self.setup_predict()
        text = self.clean_text(string)
        if text:
            # prepare our text into tokenized sequence
            inputs = self.tokenizer(text, padding = True, truncation = True, max_length = self.max_length, return_tensors = "pt").to(self.device)
            # perform inference to our model
            outputs = self.model(**inputs)
            # get output probabilities by doing softmax
            probs = outputs[0].softmax(1)
            #Could probably be done better than this
            probs = torch.Tensor([p if p > 0.50 else 0 for p in probs[0]])
            # executing argmax function to get the candidate label
            try: return self.mapping["integer"][probs.argmax().item()]
            except: return self.mapping["integer"][str(probs.argmax().item())]
        else: return ""

if __name__ == "__main__":
    confirm = input("Training will only work if you have CUDA (11.4) enabled. To get CUDA you need a compatible NVIDIA GPU. Confirm with 'y' to acknowledge: ")
    if confirm == "y":
        sent = SentimentTransformers()
        def model_init():
             return BertForSequenceClassification.from_pretrained(sent.model_name, num_labels = len(sent.mapping["string"].keys()), return_dict = True).to("cpu")
        #_int.train(autotune = True, model_init = model_init)
        sent.train()